<?php
define( 'WP_CACHE', true ); // Added by WP Rocket


// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('JETPACK_DEV_DEBUG', true);

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'karoswp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',          ')^}wzd)f@^h .jYb{M$<&ib:sw5`qr`M(QDi!so>wcK;(Xlgfi,~iCvve}T*56Z)');
define('SECURE_AUTH_KEY',   ',)5M.ts/!sFZ3Eq>o*SUY/Z/PgzMj^,jom_0Bl)=jCp- V7UTpHm+!OFMqMtEKeo');
define('LOGGED_IN_KEY',     '7SeS,)45i622)hARG?c3 eSVeAadU.s{8Y .`tTpU?{q)E< pUPu@2UYGaSLo6ly');
define('NONCE_KEY',         '#+;R>)#@:Kz(5}O22c;~{UZA{v]+O%DMxCK|:6lY+v=AKoR)!y }%&(L@e<^/{M2');
define('AUTH_SALT',         'Pl1}h{O9Q4>ojle0&}ypX:1pb0|0o[s+([[-$qYSXlT3v8(l9IMgR~j?S<a5!R_x');
define('SECURE_AUTH_SALT',  'QH~>hCNEDX>jM6g6sOQLBHh_or2qNc[LBRR&=9kKvE]&|xe?iM]E28yLgp0h7(|Q');
define('LOGGED_IN_SALT',    'a8>wvV=g?{^6O1nkDG=t6BI|qn`V_];>T<?kM|h3qJ=6+me#,k*wCxzWscDW5uZd');
define('NONCE_SALT',        'kV{eXV]w{/LQgT3<r>?ydg}R8do/gF3iP{0Z~WU[Y.nGHou6mi,5lTV]jE>qxlOL');
define('WP_CACHE_KEY_SALT', 'QG%uM:hxl^vT<pzcX3B]!`aQ,@)DA;s9GRMqE8I-H@8LCEd$i2+4kMSsXAvn}1Bl');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
	define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
