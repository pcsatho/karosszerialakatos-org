<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
if (is_plugin_active('divi-toolbox/divi-toolbox.php')) {
    global $submenu;
    update_option('wc_am_client_6865_deactivate_checkbox', 'off');
    update_option('wc_am_client_6865_activated', 'Activated');

    $wc_am_client_6865['wc_am_client_6865_api_key'] = 'tdteet';
    update_option('wc_am_client_6865',  $wc_am_client_6865);



    add_action('admin_menu', 'wc_am_client_6865_extra_menu', 999);
    function wc_am_client_6865_extra_menu()
    {
        remove_submenu_page('options-general.php', 'wc_am_client_6865_dashboard');
    }
}
